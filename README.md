Creative_Programming_for_Digital_Media_And_Mobile_Apps
======================================================

Repositorio para el curso online en Coursera.org Creative Programming for Digital Media &amp; Mobile Apps impartido por
los profesores Marco Gillies, Matthew Yee-King y Mick Grierson  de  University of London International Programmes.
https://www.coursera.org/course/digitalmedia
