/*
* Fist project for coursera's course Creative Programming for Digital Media & Mobile Apps
 * Date: 01/07/2013
 * By: Luis Cabrerizo Gomez
 * The idea behind the project is try to replicate the 
 * ToneMatrix by Andre Michelle (http://www.sneakytime.com/sound/)
 */

import controlP5.*;

// Arraylist of Cuadro to store the squares
ArrayList<Cuadro> cuadros = new ArrayList<Cuadro>();
ArrayList<Onda> ondas = new ArrayList<Onda>();

// Width of a square
int AnchoCuadro = 40;
// Height of a square
int AltoCuadro = 40;
// Width of the grid
int AnchoCuadricula = 0;
// Height of the grid
int AltoCuadricula = 0;
// Basic counter
int Contador = 1;
// Color of the squares
color squareColor = color(255, 255, 255, 175);

// Declaring the bar
Barrido Barra;
Opciones Opt;


ControlP5 cp5;
Textlabel EtiquetaBarra;
Textlabel EtiquetaOndas;
Textlabel VelocidadRadio;
RadioButton MovimientoBarra;
CheckBox ChoqueOndas;
CheckBox ColorOndasRand;
CheckBox BarraVisible;
Slider VelocidadBarra;
Slider VelocidadOnda;
Slider AlfaOnda;
Slider TamanyoOnda;
Button BotonLimpiar;
DropdownList TipoOnda;
CheckBox GiraCuadros;


void setup() 
{
  // Setting the size of the canvas
  size(800, 800, OPENGL);
  // Setting the width of the stroke
  strokeWeight(4);  
  // Setting the color of the stroke 
  stroke(0);  
  // Setting the framerate
  frameRate(60);

  AnchoCuadricula = width;
  AltoCuadricula = height;

  Opt = new Opciones(0, height - 80, width, 80, color(0, 0, 0, 255));
  cp5 = new ControlP5(this);

  // We create the bar
  Barra = new Barrido(0, 0, AnchoCuadricula, AltoCuadricula, AnchoCuadro, AltoCuadro, color(255, 255, 0, 150), true);

  CrearOpciones();

  ChoqueOndas.activate(0);
  MovimientoBarra.activate(0);
  BarraVisible.activate(0);
  TipoOnda.setIndex(0);
}

// Draw function
void draw() 
{
  // Error catching
  try
  {
    stroke(0);
    strokeWeight(4);    

    // Incrementing the counter
    Contador++;

    // Cleanning the background 
    background(125, 125, 125, 21);

    if (Opt.getOculto())
    {
      // Drawing the vertical lines of the grid
      for (int i=0; i<= AnchoCuadricula; i = i + AnchoCuadro)
      {
        line(i, 0, i, AltoCuadricula);
      }

      // Drawing the horizontal lines of the grid
      for (int i=0; i<= AltoCuadricula; i = i + AltoCuadro)
      {
        line(0, i, AnchoCuadricula, i);
      }
    }
    else
    {
      // Drawing the vertical lines of the grid
      for (int i=0; i<= AnchoCuadricula; i = i + AnchoCuadro)
      {
        line(i, 0, i, AltoCuadricula - Opt.getAlto());
      }

      // Drawing the horizontal lines of the grid
      for (int i=0; i<= AltoCuadricula - Opt.getAlto(); i = i + AltoCuadro)
      {
        line(0, i, AnchoCuadricula, i);
      }
    }

    // Drawing the bar
    Barra.display();

    // We check if we had some square on the grid 
    if (cuadros.size() > 0)
    {
      // We iterate the arraylist of squares
      for (int i = 0; i < cuadros.size(); i ++) 
      { 
        // Drawing the squares
        cuadros.get(i).display();

        // We check if some square has to be deleted
        if (cuadros.get(i).getMuerte()) 
        {
          //If so, delete it
          cuadros.remove(i);
        }

        // Now we check if the bar has collision with any square on the grid
        if (Barra.collision(cuadros.get(i)))
        {
          if (Opt.getOculto())
          {
            // If there is a collision, we play sound and change the color of the square
            cuadros.get(i).play();
          }
        }
        else
        {
          // Else, we change the color of the square to the original one
          cuadros.get(i).setColor(squareColor);
        }
      }
    }

    // Moving the bar 
    if (Contador % (65 - Opt.getVelocidadBarra()) == 0)
    {
      if (Opt.getOculto())
      {
        Barra.move(1);
      }
    }

    for (int i = 0; i < ondas.size(); i++) 
    {
      //Run the Onda methods
      if (Opt.getColisionOndas())
      {
        ondas.get(i).colision();
      }

      ondas.get(i).display();
      ondas.get(i).update();
    }

    for (int i = 0; i < ondas.size(); i++)
    {  
      if (ondas.get(i).getMuerte()) 
      {
        //If so, delete it
        ondas.remove(i);
      }
    }

    Opt.display();

    if (mouseY > height - 20)
    {
      Opt.setOculto(false);
    }

    if (mouseY < height - 80)
    {
      Opt.setOculto(true);
    }

    BotoneraOpciones(!Opt.getOculto());
  }
  catch(Exception e)
  {
    // We reset the counter if an out of bounds error
    // is about to happen
    Contador = 0;
  }
}

void BotoneraOpciones(boolean valor)
{
  ChoqueOndas.setVisible(valor);
  EtiquetaBarra.setVisible(valor);
  MovimientoBarra.setVisible(valor);
  EtiquetaOndas.setVisible(valor);
  ColorOndasRand.setVisible(valor);
  VelocidadBarra.setVisible(valor);
  VelocidadOnda.setVisible(valor);
  TamanyoOnda.setVisible(valor);
  VelocidadRadio.setVisible(valor);
  BotonLimpiar.setVisible(valor);
  BarraVisible.setVisible(valor);
  AlfaOnda.setVisible(valor);
  TipoOnda.setVisible(valor);

  if (valor)
  {
    if (Opt.getTipoOnda() == 0)
    {
      GiraCuadros.setVisible(false);
    }
    else
    {
      GiraCuadros.setVisible(true);
    }
  }  
  else
  {
    GiraCuadros.setVisible(valor);
  }
}


void mousePressed()
{
  // code that happens when the mouse button
  // is released
  boolean Encontrado = false;
  if (Opt.getOculto())
  {
    if (mouseX < AnchoCuadricula && mouseY < AltoCuadricula)
    {
      // We iterate over the squares stored on the arraylist
      for (int i = 0; i < cuadros.size(); i ++) 
      {
        // We check if we are clicking on a square or on the background 
        if (cuadros.get(i).getX() == mouseX / AnchoCuadro && cuadros.get(i).getY() == mouseY / AltoCuadro)
        {  
          // If we click on an existing square, we set it to death
          Encontrado = true;
          cuadros.get(i).setMuerte(true);
        }
      }

      if (!Encontrado)
      {
        // We create a new square if we don't found one on clicking
        Cuadro c = new Cuadro(mouseX / AnchoCuadro, mouseY / AltoCuadro, AnchoCuadro, AltoCuadro, color(255, 255, 255, 230), color(random(255), random(255), random(255), 125));
        // We add it to the arraylist
        cuadros.add(c);
      }
    }
  }
}


void stop()
{
  super.stop();
}


void CrearOpciones()
{

  EtiquetaBarra = cp5.addTextlabel("EtiquetaBarra")
    .setText("Bar")
      .setPosition(18, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;  

  MovimientoBarra = cp5.addRadioButton("MovimientoBarra")
    .setPosition(20, height - 55)
      .setSize(15, 15)
        .setColorForeground(color(120))
          .setColorActive(color(255))
            .setColorLabel(color(255))
              .setItemsPerRow(1)
                .setSpacingColumn(5)
                  .setSpacingRow(5)
                    .addItem("Vertical", 0)
                      .addItem("Horizontal", 1)
                        .setNoneSelectedAllowed(false)
                          ;       


  BarraVisible = cp5.addCheckBox("BarraVisible")
    .setPosition(20, height - 15)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Visible Bar", 0)
                      ;


  EtiquetaOndas = cp5.addTextlabel("EtiquetaOndas")
    .setText("Wave")
      .setPosition(18 + 100, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;


  ChoqueOndas = cp5.addCheckBox("ChoqueOndas")
    .setPosition(20 + 100, height - 55)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Waves Collision", 0)
                      ;


  ColorOndasRand = cp5.addCheckBox("ColorOndasRand")
    .setPosition(20 + 100, height - 35)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Random Colors", 0)
                      ;

  AlfaOnda = cp5.addSlider("AlfaOnda")
    .setPosition(20 + 100, height - 15)
      .setRange(0, 255)
        .setCaptionLabel("Wave's Alpha")
          .setSliderMode(Slider.FLEXIBLE)         
            .setValue(125);


  VelocidadRadio = cp5.addTextlabel("VelocidadRadio")
    .setText("Speed & Radious")
      .setPosition(18 + 275, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;                      

  VelocidadBarra = cp5.addSlider("VelocidadBarra")
    .setPosition(20 + 275, height - 55)
      .setRange(1, 60)
        .setCaptionLabel("Bar Speed")
          .setSliderMode(Slider.FLEXIBLE)
            .setValue(45);

  VelocidadOnda = cp5.addSlider("VelocidadOnda")
    .setPosition(20 + 275, height - 35)
      .setRange(1, 10)
        .setCaptionLabel("Wave Speed")
          .setSliderMode(Slider.FLEXIBLE)
            .setValue(5);

  TamanyoOnda = cp5.addSlider("TamanyoOnda")
    .setPosition(20 + 275, height - 15)
      .setRange(1, 800)
        .setCaptionLabel("Wave Radio")
          .setSliderMode(Slider.FLEXIBLE)         
            .setValue(400);

  TipoOnda = cp5.addDropdownList("TipoOnda")
    .setPosition(20 + 450, height - 45)
      .setBackgroundColor(color(190))
        .setItemHeight(20)
          .setBarHeight(15)
            .setColorBackground(color(60))
              .setColorActive(color(255, 128))
                ;

  TipoOnda.addItem("Circle", 0);
  TipoOnda.addItem("Square", 1);
  TipoOnda.captionLabel().set("Wave Type");
  TipoOnda.captionLabel().style().marginTop = 3;
  TipoOnda.captionLabel().style().marginLeft = 3;
  TipoOnda.valueLabel().style().marginTop = 3;


  GiraCuadros = cp5.addCheckBox("GiraCuadros")
    .setPosition(20 + 575, height - 62)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Spinning Waves", 0)
                      ;  


  BotonLimpiar = cp5.addButton("LimpiaGrid")
    .setPosition(width - 75, height - 60)
      .setSize(50, 20)
        .setCaptionLabel("Clear Grid")
          ;
}

public void LimpiaGrid() 
{
  for (int i = 0; i < cuadros.size(); i ++) 
  {
    cuadros.get(i).setMuerte(true);
  }
}

void VelocidadBarra(int velocidad) 
{
  Opt.setVelocidadBarra(velocidad);
}

void VelocidadOnda(int velocidad) 
{
  Opt.setVelocidadOnda(velocidad);
}

void TamanyoOnda(int tamanyo)
{
  Opt.setTamanyoOnda(tamanyo);
}

void AlfaOnda(int tamanyo)
{
  Opt.setAlfaOnda(tamanyo);
}


void controlEvent(ControlEvent theEvent) 
{
  if (theEvent.isFrom(ChoqueOndas)) 
  {
    if (ChoqueOndas.getArrayValue()[0] > 0)
    {
      Opt.setColisionOndas(true);
    }
    else
    {
      Opt.setColisionOndas(false);
    }
  }

  if (theEvent.isFrom(MovimientoBarra)) 
  {
    if (MovimientoBarra.getArrayValue()[0] > 0)
    {
      Barra.setBarridoVertical(true);
    }
    else
    {
      Barra.setBarridoVertical(false);
    }
    Barra.InicializarBarrido();
  }

  if (theEvent.isFrom(ColorOndasRand))
  {
    if (ColorOndasRand.getArrayValue()[0] > 0)
    {
      Opt.setColorOndasRand(true);
    }
    else
    {
      Opt.setColorOndasRand(false);
    }
  }

  if (theEvent.isFrom(BarraVisible))
  {
    if (BarraVisible.getArrayValue()[0] > 0)
    {
      Barra.setVisible(true);
    }
    else
    {
      Barra.setVisible(false);
    }
  }

  if (theEvent.isFrom(TipoOnda))
  {
    if (theEvent.isGroup()) 
    {
      Opt.setTipoOnda((int)theEvent.getGroup().getValue());
    }
  }

  if (theEvent.isFrom(GiraCuadros))
  {
    if (GiraCuadros.getArrayValue()[0] > 0)
    {
      Opt.setGiraCuadro(true);
    }
    else
    {
      Opt.setGiraCuadro(false);
    }
  }
}

/* 
 * Class for control the vertical bar
 * The idea is create a class with
 * all the necesary functions to control
 * the vertical bar wich allow us to set
 * the playing speed
 */

class Barrido
{
  // ValorX is the start value of the bar in the x axis
  int valorX = 0;
  // ValorY is the start value of the bar in the y axis
  int valorY = 0;
  // valorColor is the bar's color
  color valorColor;
  // BarridoHorizontal is a boolean variable to control the bar's movement direction  
  boolean BarridoVertical = true;
  // AnchoCuadro allow us to control the width of the bar in vertical movement
  int AnchoCuadro = 0;
  // AltoCuadro allow us to control the width of the bar in horizontal movement
  int AltoCuadro = 0;
  // AltoCuadricula give us the height of the grid
  int AltoCuadricula = 0;
  // AnchoCuadricula give us the height of the grid
  int AnchoCuadricula = 0;
  // Allow us to control the visibility of the barr
  boolean Visible = true;

  // Constructor for the vertical bar
  Barrido(int ValorXtemp, int ValorYtemp, int AnchoCuadriculatemp, int AltoCuadriculatemp, int AnchoCuadrotemp, int AltoCuadrotemp, color Colortemp, boolean BarridoVerticaltemp)
  {    
    valorX = ValorXtemp;
    valorY = ValorYtemp;
    valorColor = Colortemp;
    BarridoVertical = BarridoVertical;
    AnchoCuadro = AnchoCuadrotemp;
    AltoCuadro = AltoCuadrotemp;
    AltoCuadricula = AltoCuadriculatemp;
    AnchoCuadricula = AnchoCuadriculatemp;
  }

  // Display function
  void display() 
  {
    // We fill the bar with color
    if (Visible)
    {
      fill(valorColor);
    }
    else
    {
      fill(red(valorColor), green(valorColor), blue(valorColor), 0);
    }

    // We check the movement direction
    if (BarridoVertical)
    {
      // And we draw the bar
      rect(valorX * AnchoCuadro, valorY * AltoCuadro, AnchoCuadro, AltoCuadricula);
    }
    else
    {
      rect(valorX * AnchoCuadro, valorY * AltoCuadro, AnchoCuadricula, AnchoCuadro);
    }
  }

  // Function for collision detection
  boolean collision(Cuadro c)
  {
    if (BarridoVertical)
    {
      // We check if the value ValorX is equal to  the value X of one square
      if (valorX == c.getX())
      {
        // If it's so, return true
        return true;
      }
      else
      {
        // Else return false
        return false;
      }
    }
    else
    {
      if (valorY == c.getY())
      {
        // If it's so, return true
        return true;
      }
      else
      {
        // Else return false
        return false;
      }
    }
  }

  void setVisible(boolean valor)
  {
    Visible = valor;
  }

  boolean getVisible()
  {
    return Visible;
  }  

  void setBarridoVertical(boolean valor)
  {
    BarridoVertical = valor;
  }

  boolean getBarridoVertical()
  {
    return BarridoVertical;
  }  

  void setAnchoCuadro(int valor)
  {
    AnchoCuadro = valor;
  }

  int getAnchoCuadro()
  {
    return AnchoCuadro;
  }  

  void setAltoCuadro(int valor)
  {
    AltoCuadro = valor;
  }

  int getAltoCuadro()
  {
    return AltoCuadro;
  }    

  void InicializarBarrido()
  {
    valorX = 0;
    valorY = 0;
  }


  // Function to move the bar
  void move(int Incremento)
  {
    // We check in wich direcction is the bar moved
    if (BarridoVertical)
    {
      // We increase the value of valorX to move the bar horizontaly
      valorX++;

      // We check if the value has reached the limit to reset it 
      if (valorX >= AnchoCuadricula / AnchoCuadro)
      {
        valorX = 0;
      }
    }
    else
    {
      // We increase the value of valorY to move the bar verticaly
      valorY++;

      // We check if the value has reached the limit to reset it
      if (valorY >= AltoCuadricula / AltoCuadro)
      {
        valorY = 0;
      }
    }
  }
}

/* 
 * Class for control the squares
 * The idea is create a class with
 * all the necesary functions to control
 * the squares and its behaviour
 */

import ddf.minim.*;

class Cuadro
{
  // Position of the square on the x axis
  int valorX;
  // Position of the square on the y axis
  int valorY;
  // Width of the square
  int valorAncho;
  // Height of the square
  int valorAlto;
  // Color of the square  
  color colorCuadro;
  // Color of the wave we create on play
  color colorOnda;
  // Flag to remove the square
  boolean valorMatar = false;
  // Variable to control audio speed
  int ControlReproduccion = 0;
  // Speed of the bar. Passed to control audio speed
  int velocidadBarrido = 0;

  int[] Pitchs = {
    132, 147, 165, 175, 196, 220, 247, 262, 294, 330, 262, 294, 330, 349, 392, 440, 494, 523, 587, 659, 698, 330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175
  };
  int pitch = 0;

  Minim minim;
  AudioOutput out;

  // Constructor of square
  Cuadro(int ValorXtemp, int ValorYtemp, int Anchotemp, int Altotemp, color ColorCuadrotemp, color ColorOndatemp)
  {    
    minim = new Minim(this);
    // get a line out from Minim, default bufferSize is 1024, default sample rate is 44100, bit depth is 16
    out = minim.getLineOut(Minim.STEREO);  

    valorX = ValorXtemp;
    valorY = ValorYtemp; 
    valorAncho = Anchotemp; 
    valorAlto = Altotemp; 
    colorCuadro = ColorCuadrotemp;
    colorOnda = ColorOndatemp;

    pitch = Pitchs[(int) map(ValorYtemp, 0, int(height /Altotemp), 0, Pitchs.length)];
  }

  // We return the value of valorX
  int getX()
  {
    return valorX;
  }

  // We return the value of valorY
  int getY()
  {
    return valorY;
  }

  // We return the value of valorAncho
  int getAncho()
  {
    return valorAncho;
  }

  // We return the value of valorAlto
  int getAlto()
  {
    return valorAlto;
  }

  // We return the value of colorCuadro
  color getColor()
  {
    return colorCuadro;
  }  

  // We return the value of valorMatar
  boolean getMuerte() 
  {
    return valorMatar;
  }    

  // We set the value of valorMatar
  void setMuerte(boolean Valor)
  {
    valorMatar = Valor;
  }

  // We set the value of colorCuadro
  void setColor(color nuevoColor)
  {
    colorCuadro = nuevoColor;
  }

  // Makes the sound play and change the color of the square randomly
  void play() 
  {

    // To avoid the replay of sounds due to doubling the speed, we check
    // if ControlReproduccion is less or equal to zero
    if (ControlReproduccion <= 0)
    {

      Nota nuevaNota;

      if (pitch > 0) 
      {
        nuevaNota = new Nota(pitch, 0.2, out);
        Onda o;

        if (Opt.getColorOndasRand())
        {
          o = new Onda(valorX, valorY, valorAlto, valorAncho, color(random(255), random(255), random(255), 255));
        }
        else
        {
          o = new Onda(valorX, valorY, valorAlto, valorAncho, colorOnda);
        }
        ondas.add(o);
      }      

      // And set the delay with the speed of the bar and 2, which is the max speed the
      // sound can get
      ControlReproduccion = (65 - velocidadBarrido);

      colorCuadro = color(red(colorCuadro), green(colorCuadro), blue(colorCuadro), 255);
    }
    else
    {
      // Decreasing ControlReproduccion        
      ControlReproduccion--;
    }
  }

  // Display function   
  void display() 
  {

    velocidadBarrido = Opt.getVelocidadBarra();

    // Fill the square with the color
    fill(colorCuadro);

    // Draw the square
    rect(valorX * valorAncho, valorY * valorAlto, valorAncho, valorAlto);
  }

  void stop()
  {
    // always close Minim audio classes when you are done with them
    out.close();
    minim.stop();
  }
}

import ddf.minim.signals.*;

class Nota implements AudioSignal
{
  private float freq;
  private float nivel;
  private float alph;
  private SineWave sine;
  AudioOutput out;

  Nota(float tono, float amplitud, AudioOutput salida)
  {
    out = salida;
    freq = tono;
    nivel = amplitud;

    sine = new SineWave(freq, nivel, out.sampleRate());
    sine = new SineWave(2 * freq, nivel, out.sampleRate());
    sine = new SineWave(3 * freq, nivel, out.sampleRate());

    alph = 0.9;  // Decay constant for the envelope

    out.addSignal(this);
  }

  void updateLevel()
  {
    // Called once per buffer to decay the amplitude away
    nivel = nivel * alph;
    sine.setAmp(nivel);

    // This also handles stopping this oscillator when its level is very low.
    if (nivel < 0.01) 
    {
      out.removeSignal(this);
    }
    // this will lead to destruction of the object, since the only active 
    // reference to it is from the LineOut
  }

  void generate(float [] samp)
  {
    // generate the next buffer's worth of sinusoid
    sine.generate(samp);
    // decay the amplitude a little bit more
    updateLevel();
  }

  // AudioSignal requires both mono and stereo generate functions
  void generate(float [] sampL, float [] sampR)
  {
    sine.generate(sampL, sampR);
    updateLevel();
  }
}

class Onda
{
  int valorX = 0;
  int valorY = 0;
  color colorOnda;
  int farOut = 0;
  int valorAltoCuadro;
  int valorAnchoCuadro;
  int comprabacion = 0;
  boolean Muerte = false;
  int ContadorGiro = 0;



  Onda(int valorXtemp, int valorYtemp, int valorAltoCuadrotemp, int valorAnchoCuadrotemp, color colorOndatemp)
  {
    valorX = valorXtemp;
    valorY = valorYtemp;
    colorOnda = colorOndatemp;
    valorAltoCuadro = valorAltoCuadrotemp;
    valorAnchoCuadro = valorAnchoCuadrotemp;
    farOut = valorAnchoCuadro;

    ellipseMode(RADIUS);
  }

  void update() 
  {
    //Increase the distance out
    farOut += Opt.getVelocidadOnda();

    if (farOut > Opt.getTamanyoOnda()) 
    {
      //If so, return true
      Muerte = true;
    }
  }

  void display() 
  {

    strokeWeight(1);

    fill(colorOnda, Opt.getAlfaOnda());

    switch(Opt.getTipoOnda())
    {
    case 0:
      //Draw the ellipse
      ellipse((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2), farOut, farOut);
      break;
    case 1:
      rectMode(RADIUS);

      if (!Opt.getGiraCuadro())
      {
        rect((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2), farOut, farOut);
      }
      else
      {

        ContadorGiro += 1;
        if (ContadorGiro >= 30)
        {
          ContadorGiro = 0;
        }

        pushMatrix();
        translate((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2));
        rotate(map(ContadorGiro, 0, 30, 0, TWO_PI));
        rect(0, 0, farOut, farOut);
        popMatrix();
      }

      rectMode(CORNER);
      break;
    }
  }

  void colision() 
  {
    boolean Borrado;

    for (int i = 0; i < ondas.size(); i++) 
    {
      Borrado = false;

      if (ondas.get(i).getX() != valorX || ondas.get(i).getY() != valorY)
      {
        int Eleccion = 0;

        switch(Opt.getTipoOnda())
        {
        case 0:
          Eleccion = 0;
          break;
        case 1:
          if (Opt.getGiraCuadro())
          {
            Eleccion = 1;
          }
          else
          {
            Eleccion = 2;
          }
          break;
        }

        float dx;
        float dy;
        float distance;
        float minDist;

        switch(Eleccion)
        {
        case 0:        
          dx = (ondas.get(i).getX() * valorAnchoCuadro)  - (valorX * valorAnchoCuadro);
          dy = (ondas.get(i).getY() * valorAltoCuadro) - (valorY * valorAltoCuadro);
          distance = sqrt(dx*dx + dy*dy);
          minDist = ondas.get(i).getFarOut() + farOut;

          if (distance < minDist)
          {
            Borrado = true;
          }

          break;

        case 1:

          dx = (ondas.get(i).getX() * valorAnchoCuadro)  - (valorX * valorAnchoCuadro);
          dy = (ondas.get(i).getY() * valorAltoCuadro) - (valorY * valorAltoCuadro);
          distance = sqrt(dx*dx + dy*dy);
          minDist = sqrt((ondas.get(i).getFarOut() * ondas.get(i).getFarOut()) + (farOut * farOut));

          if (distance < minDist)
          {
            Borrado = true;
          }          

        case 2:

          rectMode(CENTER);

          boolean OutsideBottom = ((ondas.get(i).getX() * valorAltoCuadro) + (ondas.get(i).getFarOut())) < ((valorX * valorAnchoCuadro) - farOut);
          boolean OutsideTop = ((ondas.get(i).getX() * valorAltoCuadro) - (ondas.get(i).getFarOut())) > ((valorX * valorAnchoCuadro) + farOut);
          boolean OutsideLeft = ((ondas.get(i).getY() * valorAnchoCuadro) - (ondas.get(i).getFarOut())) > ((valorY * valorAltoCuadro) + farOut);
          boolean OutsideRight = ((ondas.get(i).getY() * valorAnchoCuadro) + (ondas.get(i).getFarOut())) < ((valorY * valorAltoCuadro) - farOut);

          Borrado = !(OutsideBottom || OutsideTop || OutsideLeft || OutsideRight);

          rectMode(CORNER);            

          break;
        }

        if (Borrado) 
        {
          ondas.get(i).setMuerte(true);
          Muerte = true;
        }
      }
    }
  }  

  int getFarOut()
  {
    return farOut;
  }

  int getX()
  {
    return valorX;
  }

  int getY()
  {
    return valorY;
  }  

  void setMuerte(boolean valor)
  {
    Muerte = valor;
  }

  boolean getMuerte() 
  {
    return Muerte;
  }
}

class Opciones
{

  int posX = 0;
  int posY = 0;
  int Ancho = 0;
  int Alto = 0;
  color Color;
  boolean Oculto = true;
  boolean ColisionOndas = true;
  boolean ColorOndasRand = false;
  int VelocidadBarra = 0;
  int VelocidadOnda = 0;
  int TamanyoOnda = 0;
  boolean OndaLlena = true;
  int AlfaOnda = 125;
  int TipoOnda = 0;
  boolean GiraCuadro = false;

  Opciones(int posXtemp, int posYtemp, int Anchotemp, int Altotemp, color Colortemp)
  {
    posX = posXtemp;
    posY = posYtemp;
    Ancho = Anchotemp;
    Alto = Altotemp;
    Color = Colortemp;
  }    

  void display()
  {
    if (!Oculto)
    {
      fill(Color);
      rect(posX, posY, Ancho, Alto);
    }
  }

  boolean getOculto()
  {
    return Oculto;
  }

  void setOculto(boolean valor)
  {
    Oculto = valor;
  }

  int getX()
  {
    return posX;
  }

  void setX(int valor)
  {
    posX = valor;
  }

  int getY()
  {
    return posY;
  }

  void setY(int valor)
  {
    posY = valor;
  }    

  int getAncho()
  {
    return Ancho;
  }

  void setAncho(int valor)
  {
    Ancho = valor;
  }  

  int getAlto()
  {
    return Alto;
  }

  void setAlto(int valor)
  {
    Alto = valor;
  }  

  boolean getColisionOndas()
  {
    return ColisionOndas;
  }

  void setColisionOndas(boolean valor)
  {
    ColisionOndas = valor;
  }

  boolean getColorOndasRand()
  {
    return ColorOndasRand;
  }

  void setColorOndasRand(boolean valor)
  {
    ColorOndasRand = valor;
  }

  void setVelocidadBarra(int valor)
  {
    VelocidadBarra = valor;
  }

  int getVelocidadBarra()
  {
    return VelocidadBarra;
  } 

  void setVelocidadOnda(int valor)
  {
    VelocidadOnda = valor;
  }

  int getVelocidadOnda()
  {
    return VelocidadOnda;
  }   

  void setTamanyoOnda(int valor)
  {
    TamanyoOnda = valor;
  }

  int getTamanyoOnda()
  {
    return TamanyoOnda;
  }     

  void setAlfaOnda(int valor)
  {
    AlfaOnda = valor;
  }

  int getAlfaOnda() 
  {
    return AlfaOnda;
  }  

  void setTipoOnda(int valor)
  {
    TipoOnda = valor;
  }

  int getTipoOnda() 
  {
    return TipoOnda;
  }  

  void setGiraCuadro(boolean valor)
  {
    GiraCuadro = valor;
  }

  boolean getGiraCuadro() 
  {
    return GiraCuadro;
  }
}


