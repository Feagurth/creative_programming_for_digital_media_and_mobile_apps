/*
* Second project for coursera's course Creative Programming for Digital Media & Mobile Apps
 * Date: 22/07/2013
 * By: Luis Cabrerizo Gomez
 * In: Processing
 * The idea behind the project is try to replicate the 
 * ToneMatrix by Andre Michelle (http://www.sneakytime.com/sound/)
 * but with new options and give the user more freedom to tweak the
 * way the app runs
 */

import controlP5.*;

// Arraylist of Cuadro to store the squares
ArrayList<Cuadro> cuadros = new ArrayList<Cuadro>();
ArrayList<Onda> ondas = new ArrayList<Onda>();

// Width of a square
int AnchoCuadro = 40;
// Height of a square
int AltoCuadro = 40;
// Width of the grid
int AnchoCuadricula = 0;
// Height of the grid
int AltoCuadricula = 0;
// Basic counter
int Contador = 1;
// Color of the squares
color squareColor = color(255, 255, 255, 175);

// Declaring the bar
Barrido Barra;
// Declaring the options class
Opciones Opt;


// Variables for the controls on the options panel
ControlP5 cp5;
Textlabel EtiquetaBarra;
Textlabel EtiquetaOndas;
Textlabel VelocidadRadio;
RadioButton MovimientoBarra;
CheckBox ChoqueOndas;
CheckBox ColorOndasRand;
CheckBox BarraVisible;
Slider VelocidadBarra;
Slider VelocidadOnda;
Slider AlfaOnda;
Slider TamanyoOnda;
Button BotonLimpiar;
DropdownList TipoOnda;
CheckBox GiraCuadros;
Slider VelovidadGiro;

void setup() 
{
  // Setting the size of the canvas
  size(800, 800, OPENGL);
  // Setting the width of the stroke
  strokeWeight(4);  
  // Setting the color of the stroke 
  stroke(0);  
  // Setting the framerate
  frameRate(60);

  AnchoCuadricula = width;
  AltoCuadricula = height;

  // Creating the options object
  Opt = new Opciones(0, height - 80, width, 80, color(0, 0, 0, 255));

  cp5 = new ControlP5(this);

  // We create the bar
  Barra = new Barrido(0, 0, AnchoCuadricula, AltoCuadricula, AnchoCuadro, AltoCuadro, color(255, 255, 0, 150), true);

  // We call the function responsible for creating the buttons on the option panel
  CrearBotones();

  // We initialize the buttons with the correct options 
  ChoqueOndas.activate(0);
  MovimientoBarra.activate(0);
  BarraVisible.activate(0);
  TipoOnda.setIndex(0);
}

// Draw function
void draw() 
{
  // Error catching
  try
  {
    stroke(0);
    strokeWeight(4);    

    // Incrementing the counter
    Contador++;

    // Cleanning the background 
    background(125, 125, 125, 21);

    if (Opt.getOculto())
    {
      // Drawing the vertical lines of the grid
      for (int i=0; i<= AnchoCuadricula; i = i + AnchoCuadro)
      {
        line(i, 0, i, AltoCuadricula);
      }

      // Drawing the horizontal lines of the grid
      for (int i=0; i<= AltoCuadricula; i = i + AltoCuadro)
      {
        line(0, i, AnchoCuadricula, i);
      }
    }
    else
    {
      // Drawing the vertical lines of the grid
      for (int i=0; i<= AnchoCuadricula; i = i + AnchoCuadro)
      {
        line(i, 0, i, AltoCuadricula - Opt.getAlto());
      }

      // Drawing the horizontal lines of the grid
      for (int i=0; i<= AltoCuadricula - Opt.getAlto(); i = i + AltoCuadro)
      {
        line(0, i, AnchoCuadricula, i);
      }
    }

    // Drawing the bar
    Barra.display();

    // We check if we had some square on the grid 
    if (cuadros.size() > 0)
    {
      // We iterate the arraylist of squares
      for (int i = 0; i < cuadros.size(); i ++) 
      { 
        // Drawing the squares
        cuadros.get(i).display();

        // We check if some square has to be deleted
        if (cuadros.get(i).getMuerte()) 
        {
          //If so, delete it
          cuadros.remove(i);
        }

        // Now we check if the bar has collision with any square on the grid
        if (Barra.collision(cuadros.get(i)))
        {
          if (Opt.getOculto())
          {
            // If there is a collision, we play sound and change the color of the square
            cuadros.get(i).play();
          }
        }
        else
        {
          // Else, we change the color of the square to the original one
          cuadros.get(i).setColor(squareColor);
        }
      }
    }

    // We iterate through the array of waves
    for (int i = 0; i < ondas.size(); i++) 
    {
      //Run the Onda methods

      // We check if the collision between waves is activated, if so, we run wave's collision
      if (Opt.getColisionOndas())
      {
        ondas.get(i).colision();
      }

      ondas.get(i).display();
      ondas.get(i).update();
    }

    // We iterate again through the array of waves 
    for (int i = 0; i < ondas.size(); i++)
    {  
      // We check if some wave has to be deleted
      if (ondas.get(i).getMuerte()) 
      {
        //If so, delete it
        ondas.remove(i);
      }
    }

    // We draw the options panel
    Opt.display();

    // And check if we have to show the panel or hide it
    if (mouseY > height - 20)
    {
      Opt.setOculto(false);
    }
    if (mouseY < height - 80)
    {
      Opt.setOculto(true);
    }

    // Now we show or hide the buttons on the option panel based on the visibility of the option panel itself
    BotoneraOpciones(!Opt.getOculto());
    

    // Moving the bar 
    if (Contador % (62 - Opt.getVelocidadBarra()) == 0)
    {
      // We check if the bar must be drawn
      if (Opt.getOculto())
      {
        Barra.move(1);
      }
    }    
    
  }
  catch(Exception e)
  {
    // We reset the counter if an out of bounds error
    // is about to happen
    Contador = 0;
  }
}

// Function to hide or show the option buttons on the options panel
void BotoneraOpciones(boolean valor)
{

  // Hide or show
  ChoqueOndas.setVisible(valor);
  EtiquetaBarra.setVisible(valor);
  MovimientoBarra.setVisible(valor);
  EtiquetaOndas.setVisible(valor);
  ColorOndasRand.setVisible(valor);
  VelocidadBarra.setVisible(valor);
  VelocidadOnda.setVisible(valor);
  TamanyoOnda.setVisible(valor);
  VelocidadRadio.setVisible(valor);
  BotonLimpiar.setVisible(valor);
  BarraVisible.setVisible(valor);
  AlfaOnda.setVisible(valor);
  TipoOnda.setVisible(valor);
  VelovidadGiro.setVisible(valor);


  // If the dropdown box to select the type of wave has squared value selected and the buttons are visible
  // we show the button to enable spinning squares and the slider to adjust the spinning speed
  if (valor)
  {
    if (Opt.getTipoOnda() == 0)
    {
      GiraCuadros.setVisible(false);
      VelovidadGiro.setVisible(false);
    }
    else
    {
      GiraCuadros.setVisible(true);
      VelovidadGiro.setVisible(true);
    }
  }  
  else
  {
    GiraCuadros.setVisible(valor);
    VelovidadGiro.setVisible(valor);
  }
}


void mousePressed()
{
  boolean Encontrado = false;

  // We check if the option panel is hidden
  if (Opt.getOculto())
  {
    // If its hidden every click will create a  square or will delete it
    if (mouseX < AnchoCuadricula && mouseY < AltoCuadricula)
    {
      // We iterate over the squares stored on the arraylist
      for (int i = 0; i < cuadros.size(); i ++) 
      {
        // We check if we are clicking on a square or on the background 
        if (cuadros.get(i).getX() == mouseX / AnchoCuadro && cuadros.get(i).getY() == mouseY / AltoCuadro)
        {  
          // If we click on an existing square, we set it to death
          Encontrado = true;
          cuadros.get(i).setMuerte(true);
        }
      }

      if (!Encontrado)
      {
        // We create a new square if we don't found one on clicking
        Cuadro c = new Cuadro(mouseX / AnchoCuadro, mouseY / AltoCuadro, AnchoCuadro, AltoCuadro, color(255, 255, 255, 230), color(random(255), random(255), random(255), 125));
        // We add it to the arraylist
        cuadros.add(c);
      }
    }
  }
}


void stop()
{
  super.stop();
}


// Function to create the option buttons
void CrearBotones()
{

  // Label for text
  EtiquetaBarra = cp5.addTextlabel("EtiquetaBarra")
    .setText("Bar")
      .setPosition(18, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;  

  // Radio button to select the movement direction of the bar
  MovimientoBarra = cp5.addRadioButton("MovimientoBarra")
    .setPosition(20, height - 55)
      .setSize(15, 15)
        .setColorForeground(color(120))
          .setColorActive(color(255))
            .setColorLabel(color(255))
              .setItemsPerRow(1)
                .setSpacingColumn(5)
                  .setSpacingRow(5)
                    .addItem("Vertical", 0)
                      .addItem("Horizontal", 1)
                        .setNoneSelectedAllowed(false)
                          ;       


  // Button to make the bar visible or invisible
  BarraVisible = cp5.addCheckBox("BarraVisible")
    .setPosition(20, height - 15)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Visible Bar", 0)
                      ;


  // Label for text
  EtiquetaOndas = cp5.addTextlabel("EtiquetaOndas")
    .setText("Wave")
      .setPosition(18 + 100, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;


  // Button to enable or disable collision detection on que waves
  ChoqueOndas = cp5.addCheckBox("ChoqueOndas")
    .setPosition(20 + 100, height - 55)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Waves Collision", 0)
                      ;


  // Button to enable or disable the random color of waves on every played sound
  ColorOndasRand = cp5.addCheckBox("ColorOndasRand")
    .setPosition(20 + 100, height - 35)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Random Colors", 0)
                      ;

  // Slider to select the alpha of the filling color of the wave
  AlfaOnda = cp5.addSlider("AlfaOnda")
    .setPosition(20 + 100, height - 15)
      .setRange(0, 255)
        .setCaptionLabel("Wave's Alpha")
          .setSliderMode(Slider.FLEXIBLE)         
            .setValue(125);


  // Label for text
  VelocidadRadio = cp5.addTextlabel("VelocidadRadio")
    .setText("Speed & Radious")
      .setPosition(18 + 275, height - 75)
        .setColor(255) 
          .setFont(createFont("Sans Serif", 14))
            ;                      

  // Slider to select the movement speed of the bar
  VelocidadBarra = cp5.addSlider("VelocidadBarra")
    .setPosition(20 + 275, height - 55)
      .setRange(1, 60)
        .setCaptionLabel("Bar Speed")
          .setSliderMode(Slider.FLEXIBLE)
            .setValue(45);

  // Slider to select the propagation  speed of the waves
  VelocidadOnda = cp5.addSlider("VelocidadOnda")
    .setPosition(20 + 275, height - 35)
      .setRange(1, 10)
        .setCaptionLabel("Wave Speed")
          .setSliderMode(Slider.FLEXIBLE)
            .setValue(5);

  // Slider to select the maximum size of the waves
  TamanyoOnda = cp5.addSlider("TamanyoOnda")
    .setPosition(20 + 275, height - 15)
      .setRange(1, 800)
        .setCaptionLabel("Wave Radio")
          .setSliderMode(Slider.FLEXIBLE)         
            .setValue(400);

  // Dropdown list to allow us to select the type of wave
  TipoOnda = cp5.addDropdownList("TipoOnda")
    .setPosition(20 + 450, height - 45)
      .setBackgroundColor(color(190))
        .setItemHeight(20)
          .setBarHeight(15)
            .setColorBackground(color(60))
              .setColorActive(color(255, 128))
                ;

  TipoOnda.addItem("Circle", 0);
  TipoOnda.addItem("Square", 1);
  TipoOnda.captionLabel().set("Wave Type");
  TipoOnda.captionLabel().style().marginTop = 3;
  TipoOnda.captionLabel().style().marginLeft = 3;
  TipoOnda.valueLabel().style().marginTop = 3;


  // Button to enable or disable the spinning squared waves 
  GiraCuadros = cp5.addCheckBox("GiraCuadros")
    .setPosition(20 + 575, height - 62)
      .setColorForeground(color(120))
        .setColorActive(color(255))
          .setColorLabel(color(255))
            .setSize(15, 15)
              .setItemsPerRow(1)
                .setSpacingColumn(30)
                  .setSpacingRow(20)
                    .addItem("Spinning Waves", 0)
                      ;  

  // Slider to select the spinning velocity of the squared waves
  VelovidadGiro = cp5.addSlider("VelovidadGiro")
    .setPosition(20 + 575, height - 35)
      .setRange(1, 10)
        .setCaptionLabel("Spin's Speed")
          .setSliderMode(Slider.FLEXIBLE)         
            .setValue(5);


  // Button to clean the grid
  BotonLimpiar = cp5.addButton("LimpiaGrid")
    .setPosition(width - 75, height - 65)
      .setSize(50, 20)
        .setCaptionLabel("Clear Grid")
          ;
}

// Event function for BotonLimpiar
public void LimpiaGrid() 
{
  for (int i = 0; i < cuadros.size(); i ++) 
  {
    cuadros.get(i).setMuerte(true);
  }
}

// Event function for VelocidadBarra
void VelocidadBarra(int velocidad) 
{
  Opt.setVelocidadBarra(velocidad);
}

// Event function for VelocidadOnda
void VelocidadOnda(int velocidad) 
{
  Opt.setVelocidadOnda(velocidad);
}

// Event function for TamanyoOnda
void TamanyoOnda(int tamanyo)
{
  Opt.setTamanyoOnda(tamanyo);
}

// Event function for AlfaOnda
void AlfaOnda(int tamanyo)
{
  Opt.setAlfaOnda(tamanyo);
}

// Event function for VelovidadGiro
void VelovidadGiro(int velocidad)
{
  Opt.setVelocidadGiro(velocidad);
}

// Event function for the rest of buttons
void controlEvent(ControlEvent theEvent) 
{
  // We check who launch the event
  // and make the necesary changes
  // to the app behaviour

  // If the event comes from ChoqueOndas
  if (theEvent.isFrom(ChoqueOndas)) 
  {
    // We check if the button is enabled
    if (ChoqueOndas.getArrayValue()[0] > 0)
    {
      // If so, we enabled the waves collision on the options class
      Opt.setColisionOndas(true);
    }
    else
    {
      // In the other hand, we disable the waves collision on the options class
      Opt.setColisionOndas(false);
    }
  }

  // If the event comes from MovimientoBarra
  if (theEvent.isFrom(MovimientoBarra)) 
  {
    // We check if the button is enabled
    if (MovimientoBarra.getArrayValue()[0] > 0)
    {
      // If so, we set the bar movements on vertical
      Barra.setBarridoVertical(true);
    }
    else
    {
      // In the other hand, we set the bar movements on horizontal
      Barra.setBarridoVertical(false);
    }

    // We initialize the bar after the changes
    Barra.InicializarBarrido();
  }

  // If the event comes from ColorOndasRand
  if (theEvent.isFrom(ColorOndasRand))
  {
    // We check if the button is enabled
    if (ColorOndasRand.getArrayValue()[0] > 0)
    {
      // If so, we enabled the random color of waves on the options class
      Opt.setColorOndasRand(true);
    }
    else
    {
      // In the other hand, we disable the random color of waves on the options class
      Opt.setColorOndasRand(false);
    }
  }

  // If the event comes from BarraVisible
  if (theEvent.isFrom(BarraVisible))
  {
    // We check if the button is enabled
    if (BarraVisible.getArrayValue()[0] > 0)
    {
      // If so, we enabled the visibility of the bar on the options class
      Barra.setVisible(true);
    }
    else
    {
      // In the other hand, we disable the visibility of the bar on the options class
      Barra.setVisible(false);
    }
  }

  // If the event comes from TipoOnda
  if (theEvent.isFrom(TipoOnda))
  {
    // We chek if the event comes from a group
    if (theEvent.isGroup()) 
    {
      // We pass the value to the options class to se the type of the waves 
      Opt.setTipoOnda((int)theEvent.getGroup().getValue());
    }
  }

  // If the event comes from GiraCuadros
  if (theEvent.isFrom(GiraCuadros))
  {
    // We check if the button is enabled
    if (GiraCuadros.getArrayValue()[0] > 0)
    {
      // If so, we enabled the spinnig squares on the options class
      Opt.setGiraCuadro(true);
    }
    else
    {
      // In the other hand, we disable the spinnig squares on the options class
      Opt.setGiraCuadro(false);
    }
  }
}

