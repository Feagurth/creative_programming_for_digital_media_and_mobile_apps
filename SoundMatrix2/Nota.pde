/*
*  Class for generating sounds with Minim
 */
import ddf.minim.signals.*;

class Nota implements AudioSignal
{
  // Frequency variable
  private float freq;
  // Amplitude variable
  private float nivel;
  // Decay variable
  private float alph;
  
  private SineWave sine;
  AudioOutput out;

  // Constructor the Nota class
  Nota(float tono, float amplitud, AudioOutput salida)
  {
    out = salida;
    freq = tono;
    nivel = amplitud;

    // We create a new SineWave with the frequency and the amplitude
    sine = new SineWave(freq, nivel, out.sampleRate());

    alph = 0.9;  // Decay constant for the envelope

    out.addSignal(this);
  }

  void updateLevel()
  {
    // Called once per buffer to decay the amplitude away
    nivel = nivel * alph;
    sine.setAmp(nivel);

    // This also handles stopping this oscillator when its level is very low.
    if (nivel < 0.01) 
    {
      out.removeSignal(this);
    }
    // this will lead to destruction of the object, since the only active 
    // reference to it is from the LineOut
  }

  void generate(float [] samp)
  {
    // generate the next buffer's worth of sinusoid
    sine.generate(samp);
    // decay the amplitude a little bit more
    updateLevel();
  }

  // AudioSignal requires both mono and stereo generate functions
  void generate(float [] sampL, float [] sampR)
  {
    sine.generate(sampL, sampR);
    updateLevel();
  }
}

