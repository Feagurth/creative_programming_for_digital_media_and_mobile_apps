/* 
 * Class for control the vertical bar
 * The idea is create a class with
 * all the necesary functions to control
 * the vertical bar wich allow us to set
 * the playing speed
 */

class Barrido
{
  // ValorX is the start value of the bar in the x axis
  int valorX = 0;
  // ValorY is the start value of the bar in the y axis
  int valorY = 0;
  // valorColor is the bar's color
  color valorColor;
  // BarridoHorizontal is a boolean variable to control the bar's movement direction  
  boolean BarridoVertical = true;
  // AnchoCuadro allow us to control the width of the bar in vertical movement
  int AnchoCuadro = 0;
  // AltoCuadro allow us to control the width of the bar in horizontal movement
  int AltoCuadro = 0;
  // AltoCuadricula give us the height of the grid
  int AltoCuadricula = 0;
  // AnchoCuadricula give us the height of the grid
  int AnchoCuadricula = 0;
  // Allow us to control the visibility of the bar
  boolean Visible = true;

  // Constructor for the vertical bar
  Barrido(int ValorXtemp, int ValorYtemp, int AnchoCuadriculatemp, int AltoCuadriculatemp, int AnchoCuadrotemp, int AltoCuadrotemp, color Colortemp, boolean BarridoVerticaltemp)
  {    
    valorX = ValorXtemp;
    valorY = ValorYtemp;
    valorColor = Colortemp;
    BarridoVertical = BarridoVertical;
    AnchoCuadro = AnchoCuadrotemp;
    AltoCuadro = AltoCuadrotemp;
    AltoCuadricula = AltoCuadriculatemp;
    AnchoCuadricula = AnchoCuadriculatemp;
  }

  // Display function
  void display() 
  {
    // We fill the bar with color
    if (Visible)
    {
      fill(valorColor);
    }
    else
    {
      fill(red(valorColor), green(valorColor), blue(valorColor), 0);
    }

    // We check the movement direction
    if (BarridoVertical)
    {
      // And we draw the bar
      rect(valorX * AnchoCuadro, valorY * AltoCuadro, AnchoCuadro, AltoCuadricula);
    }
    else
    {
      rect(valorX * AnchoCuadro, valorY * AltoCuadro, AnchoCuadricula, AnchoCuadro);
    }
  }

  // Function for collision detection
  boolean collision(Cuadro c)
  {
    if (BarridoVertical)
    {
      // We check if the value ValorX is equal to  the value X of one square
      if (valorX == c.getX())
      {
        // If it's so, return true
        return true;
      }
      else
      {
        // Else return false
        return false;
      }
    }
    else
    {
      if (valorY == c.getY())
      {
        // If it's so, return true
        return true;
      }
      else
      {
        // Else return false
        return false;
      }
    }
  }


  // We set the value of Visible  
  void setVisible(boolean valor)
  {
    Visible = valor;
  }

  // We return the value of Visible
  boolean getVisible()
  {
    return Visible;
  }  

  // We set the value of BarridoVertical
  void setBarridoVertical(boolean valor)
  {
    BarridoVertical = valor;
  }

  // We return the value of BarridoVertical
  boolean getBarridoVertical()
  {
    return BarridoVertical;
  }  

  // We set the value of AnchoCuadro
  void setAnchoCuadro(int valor)
  {
    AnchoCuadro = valor;
  }

  // We return the value of AnchoCuadro
  int getAnchoCuadro()
  {
    return AnchoCuadro;
  }  

  // We set the value of AltoCuadro
  void setAltoCuadro(int valor)
  {
    AltoCuadro = valor;
  }

  // We return the value of AltoCuadro
  int getAltoCuadro()
  {
    return AltoCuadro;
  }    

  // Function for reinitialize the x and y values of the bar
  void InicializarBarrido()
  {
    valorX = 0;
    valorY = 0;
  }

  // Function to move the bar
  void move(int Incremento)
  {
    // We check in wich direcction is the bar moved
    if (BarridoVertical)
    {
      // We increase the value of valorX to move the bar horizontaly
      valorX++;

      // We check if the value has reached the limit to reset it 
      if (valorX >= AnchoCuadricula / AnchoCuadro)
      {
        valorX = 0;
      }
    }
    else
    {
      // We increase the value of valorY to move the bar verticaly
      valorY++;

      // We check if the value has reached the limit to reset it
      if (valorY >= AltoCuadricula / AltoCuadro)
      {
        valorY = 0;
      }
    }
  }
}

