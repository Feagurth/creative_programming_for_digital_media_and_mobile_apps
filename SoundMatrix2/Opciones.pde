/* 
 * Class for storing the options of the app
 * The idea behind the class is allow us to 
 * store all the options of the app in the same
 * place and help us to draw the panel options
 */

class Opciones
{

  // X position of the option panel
  int posX = 0;
  // Y position of the option panel
  int posY = 0;
  // Height of the option panel
  int Ancho = 0;
  // Width of the option panel
  int Alto = 0;
  // Color of the option panel  
  color Color;
  // Control the visibility of the panel
  boolean Oculto = true;
  // Variable to control wave's collisions
  boolean ColisionOndas = true;
  // Variable to control if the wave have to generate a new color on every played note
  boolean ColorOndasRand = false;
  // Variable to control the speed of the bar
  int VelocidadBarra = 0;
  // Variable to control the propagation speed of the wave
  int VelocidadOnda = 0;
  // Variable to control the size of the wave
  int TamanyoOnda = 0;  
  // Variable to control the alpha of the wave's filling color
  int AlfaOnda = 125;
  // Variable to control the type of the wave
  int TipoOnda = 0;
  // Variable to control the rotation of the squared type waves  
  boolean GiraCuadro = false;
  // Variable to control the rotation's speed of the squared type waves 
  int VelocidadGiro = 5;

  // Constructor for Opciones class
  Opciones(int posXtemp, int posYtemp, int Anchotemp, int Altotemp, color Colortemp)
  {
    posX = posXtemp;
    posY = posYtemp;
    Ancho = Anchotemp;
    Alto = Altotemp;
    Color = Colortemp;
  }    

  void display()
  {
    // If the options panel isn't hidden, we draw it
    if (!Oculto)
    {
      fill(Color);
      rect(posX, posY, Ancho, Alto);
    }
  }


  // We return the value of Oculto
  boolean getOculto()
  {
    return Oculto;
  }

  // We set the value of Oculto
  void setOculto(boolean valor)
  {
    Oculto = valor;
  }

  // We return the value of posX
  int getX()
  {
    return posX;
  }

  // We set the value of posX
  void setX(int valor)
  {
    posX = valor;
  }

  // We return the value of posY
  int getY()
  {
    return posY;
  }

  // We set the value of posY
  void setY(int valor)
  {
    posY = valor;
  }    

  // We return the value of Ancho
  int getAncho()
  {
    return Ancho;
  }

  // We set the value of Ancho
  void setAncho(int valor)
  {
    Ancho = valor;
  }  

  // We return the value of Alto
  int getAlto()
  {
    return Alto;
  }

  // We set the value of Alto
  void setAlto(int valor)
  {
    Alto = valor;
  }  

  // We return the value of ColisionOndas
  boolean getColisionOndas()
  {
    return ColisionOndas;
  }

  // We set the value of ColisionOndas
  void setColisionOndas(boolean valor)
  {
    ColisionOndas = valor;
  }

  // We return the value of ColorOndasRand
  boolean getColorOndasRand()
  {
    return ColorOndasRand;
  }

  // We set the value of ColorOndasRand
  void setColorOndasRand(boolean valor)
  {
    ColorOndasRand = valor;
  }

  // We return the value of VelocidadBarra
  int getVelocidadBarra()
  {
    return VelocidadBarra;
  } 

  // We set the value of VelocidadBarra
  void setVelocidadBarra(int valor)
  {
    VelocidadBarra = valor;
  }

  // We return the value of VelocidadOnda
  int getVelocidadOnda()
  {
    return VelocidadOnda;
  }   

  // We set the value of VelocidadOnda
  void setVelocidadOnda(int valor)
  {
    VelocidadOnda = valor;
  }

  // We return the value of TamanyoOnda
  int getTamanyoOnda()
  {
    return TamanyoOnda;
  }     

  // We set the value of TamanyoOnda
  void setTamanyoOnda(int valor)
  {
    TamanyoOnda = valor;
  }

  // We return the value of AlfaOnda
  int getAlfaOnda() 
  {
    return AlfaOnda;
  }

  // We set the value of AlfaOnda
  void setAlfaOnda(int valor)
  {
    AlfaOnda = valor;
  }

  // We return the value of TipoOnda
  int getTipoOnda() 
  {
    return TipoOnda;
  }  

  // We set the value of TipoOnda
  void setTipoOnda(int valor)
  {
    TipoOnda = valor;
  }

  // We return the value of GiraCuadro
  boolean getGiraCuadro() 
  {
    return GiraCuadro;
  }

  // We set the value of GiraCuadro
  void setGiraCuadro(boolean valor)
  {
    GiraCuadro = valor;
  }
  
  // We return the value of VelocidadGiro
  int getVelocidadGiro() 
  {
    return VelocidadGiro;
  }

  // We set the value of VelovidadGiro
  void setVelocidadGiro(int valor)
  {
    VelocidadGiro = valor;
  }  
}

