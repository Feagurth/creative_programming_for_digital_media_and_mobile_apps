/*
* Class for generating waves
 * The basic idea is to create a 
 * class than allow us to control how the
 * waves are created and its behaviour
 */
class Onda
{
  // Position of the wave on the x axis 
  int valorX = 0;
  // Position of the wave on the y axis
  int valorY = 0;
  // Color of the wave
  color colorOnda;
  // Size of the wave
  int Tamanyo = 0;
  // Height of the square where the wave is going to be created
  int valorAltoCuadro;
  // Width of the square where the wave is going to be created
  int valorAnchoCuadro;  
  // Variable for control the delection state of the wave
  boolean Muerte = false;
  // Counter for the spinning of the squared waves
  int ContadorGiro = 0;

  // Constructor for the wave class
  Onda(int valorXtemp, int valorYtemp, int valorAltoCuadrotemp, int valorAnchoCuadrotemp, color colorOndatemp)
  {
    valorX = valorXtemp;
    valorY = valorYtemp;
    colorOnda = colorOndatemp;
    valorAltoCuadro = valorAltoCuadrotemp;
    valorAnchoCuadro = valorAnchoCuadrotemp;

    // The initial radius of the wave is equal to the size of the square to star drawing the wave outside the square 
    Tamanyo =  valorAnchoCuadro;

    // We change the ellipseMode to radius to allow us a better collision detection routine
    ellipseMode(RADIUS);
  }

  void update() 
  {
    //We increase the distance based on the propagation speed of the wave 
    Tamanyo += Opt.getVelocidadOnda();

    // We check if the size of the wave is greater than the size stored on the options
    if (Tamanyo > Opt.getTamanyoOnda()) 
    {
      //If so, set wave for delete
      Muerte = true;
    }
  }

  void display() 
  {

    strokeWeight(1);

    // We fill the wave with the specified color an the alpha stored on the options
    fill(colorOnda, Opt.getAlfaOnda());

    // We check the type of the wave
    switch(Opt.getTipoOnda())
    {

      // If type 0, draw the wave as an circle
    case 0:   
      ellipse((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2), Tamanyo, Tamanyo);
      break;

      // If type 1, draw the wave as a square
    case 1:
      // We change the rectMode to radius to allow us a easier rotation, if needed
      rectMode(RADIUS);

      // We check in the options if the squared wave has to rotate
      if (!Opt.getGiraCuadro())
      {
        // If there is no rotation on the wave, we draw a square
        rect((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2), Tamanyo, Tamanyo);
      }
      else
      {

        // If there is rotation, we increase the rotation
        ContadorGiro += Opt.getVelocidadGiro();

        // If the rotarion is greater than 30, we reset the variable
        if (ContadorGiro >= 30)
        {
          ContadorGiro = 0;
        }

        // Now we draw the square with the rotation aided by translate and rotation
        pushMatrix();
        translate((valorX * valorAltoCuadro) + (valorAltoCuadro / 2), (valorY * valorAnchoCuadro) + (valorAnchoCuadro / 2));
        rotate(map(ContadorGiro, 0, 30, 0, TWO_PI));
        rect(0, 0, Tamanyo, Tamanyo);
        popMatrix();
      }

      // We reset the rectMode to corner to let us draw squares old style
      rectMode(CORNER);
      break;
    }
  }

  // Function for collision detection between waves
  void colision() 
  {
    boolean Borrado;

    // We iterate between every wave stored on its array
    for (int i = 0; i < ondas.size(); i++) 
    {
      Borrado = false;

      // We discard the wave on the array wich is the same as this
      if (ondas.get(i).getX() != valorX || ondas.get(i).getY() != valorY)
      {
        int Eleccion = 0;

        // Now we check the type of wave to choose the kind of collision detection
        switch(Opt.getTipoOnda())
        {

          // If the wave's type is rounded, Eleccion set to 0
        case 0:
          Eleccion = 0;
          break;
          // If the wave's type is squared, we must check if the wave is rotating
        case 1:
          if (Opt.getGiraCuadro())
          {
            // If the squared wave is rotating, Eleccion sets to 1
            Eleccion = 1;
          }
          else
          {
            // If the squared wave isn't rotating, Eleccion sets to 2
            Eleccion = 2;
          }
          break;
        }

        float dx;
        float dy;
        float distance;
        float minDist;

        switch(Eleccion)
        {
          // If the wave is circled shaped 
        case 0:
          // We calculate the horizontal distance between the origin of the waves        
          dx = (ondas.get(i).getX() * valorAnchoCuadro)  - (valorX * valorAnchoCuadro);
          // We calculate the vertical distance between the origin of the waves
          dy = (ondas.get(i).getY() * valorAltoCuadro) - (valorY * valorAltoCuadro);
          // We use Pythagorean theorem to calculate the distance between the origin of the two waves
          distance = sqrt(dx*dx + dy*dy);
          // The minimal distance for the waves to collide is equal to the sum of both it's radius
          minDist = ondas.get(i).getTamanyo() + Tamanyo;

          // If the distance is lesser than the minimal distance the waves collide
          if (distance < minDist)
          {
            // We set the waves to be deleted
            Borrado = true;
          }

          break;

          // If the wave is an squared rotating one, the collision detection is similar to the circled shaped
        case 1:
          // We calculate the horizontal distance between the origin of the waves
          dx = (ondas.get(i).getX() * valorAnchoCuadro)  - (valorX * valorAnchoCuadro);
          // We calculate the vertical distance between the origin of the waves
          dy = (ondas.get(i).getY() * valorAltoCuadro) - (valorY * valorAltoCuadro);
          // We use Pythagorean theorem to calculate the distance between the origin of the two waves
          distance = sqrt(dx*dx + dy*dy);
          // The minimal distance for to squares to collide is equal to the sum of both it's hypotenuses
          minDist = sqrt((ondas.get(i).getTamanyo() * ondas.get(i).getTamanyo()) + (Tamanyo * Tamanyo));

          // If the distance is lesser than the minimal distance the waves collide
          if (distance < minDist)
          {
            // We set the waves to be deleted
            Borrado = true;
          }          

          // If the wave is an squared one 
        case 2:

          // We change the rectMode to center to allow us easier calculations
          rectMode(CENTER);

          // We create a boolean variable to store if the bottom side of the square is below the upper side of the other square wave 
          boolean OutsideBottom = ((ondas.get(i).getX() * valorAltoCuadro) + (ondas.get(i).getTamanyo())) < ((valorX * valorAnchoCuadro) - Tamanyo);

          // We create another a boolean variable to store if the upper side of the square is on top of the lower side of the other square wave
          boolean OutsideTop = ((ondas.get(i).getX() * valorAltoCuadro) - (ondas.get(i).getTamanyo())) > ((valorX * valorAnchoCuadro) + Tamanyo);

          // We create another a boolean variable to store if the left side of the square is far on the x axis than the right side of the other square wave
          boolean OutsideLeft = ((ondas.get(i).getY() * valorAnchoCuadro) - (ondas.get(i).getTamanyo())) > ((valorY * valorAltoCuadro) + Tamanyo);

          // We create another a boolean variable to store if the right side of the square is near on the x axis than the left side of the other square wave
          boolean OutsideRight = ((ondas.get(i).getY() * valorAnchoCuadro) + (ondas.get(i).getTamanyo())) < ((valorY * valorAltoCuadro) - Tamanyo);

          // Both squares collides if any of the above afirmations turns to be true, and the waves must be deleted
          Borrado = !(OutsideBottom || OutsideTop || OutsideLeft || OutsideRight);

          // We change rectMode back to corner 
          rectMode(CORNER);            

          break;
        }

        // If the waves must be deleted, we do it
        if (Borrado) 
        {
          ondas.get(i).setMuerte(true);
          Muerte = true;
        }
      }
    }
  }  


  // We return the size of the wave
  int getTamanyo()
  {
    return Tamanyo;
  }

  // We return x position of the wave
  int getX()
  {
    return valorX;
  }

  // We return Y position of the wave
  int getY()
  {
    return valorY;
  }  

  // We return deletion state of the wave 
  boolean getMuerte() 
  {
    return Muerte;
  }

  // We set the deletion state of the wave
  void setMuerte(boolean valor)
  {
    Muerte = valor;
  }
}

