/* 
 * Class for control the squares
 * The idea is create a class with
 * all the necesary functions to control
 * the squares and its behaviour
 */

import ddf.minim.*;

class Cuadro
{
  // Position of the square on the x axis
  int valorX;
  // Position of the square on the y axis
  int valorY;
  // Width of the square
  int valorAncho;
  // Height of the square
  int valorAlto;
  // Color of the square  
  color colorCuadro;
  // Color of the wave we create on play
  color colorOnda;
  // Flag to remove the square
  boolean valorMatar = false;
  // Variable to control audio speed
  int ControlReproduccion = 0;
  // Speed of the bar. Passed to control audio speed
  int velocidadBarrido = 0;

  // Array of pitchs for generating different sounds
  int[] Pitchs = {
    132, 147, 165, 175, 196, 220, 247, 262, 294, 330, 262, 294, 330, 349, 392, 440, 494, 523, 587, 659, 698, 330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175
  };
  int pitch = 0;

  Minim minim;
  AudioOutput out;

  // Constructor of square
  Cuadro(int ValorXtemp, int ValorYtemp, int Anchotemp, int Altotemp, color ColorCuadrotemp, color ColorOndatemp)
  {    
    minim = new Minim(this);

    // Get a line out from Minim, default bufferSize is 1024, default sample rate is 44100, bit depth is 16
    out = minim.getLineOut(Minim.STEREO);  

    valorX = ValorXtemp;
    valorY = ValorYtemp; 
    valorAncho = Anchotemp; 
    valorAlto = Altotemp; 
    colorCuadro = ColorCuadrotemp;
    colorOnda = ColorOndatemp;

    // We map the vertical position of the square with the array of pitch to get a pitch 
    pitch = Pitchs[(int) map(ValorYtemp, 0, int(height /Altotemp), 0, Pitchs.length)];
  }

  // We return the value of valorX
  int getX()
  {
    return valorX;
  }

  // We return the value of valorY
  int getY()
  {
    return valorY;
  }

  // We return the value of valorAncho
  int getAncho()
  {
    return valorAncho;
  }

  // We return the value of valorAlto
  int getAlto()
  {
    return valorAlto;
  }

  // We return the value of colorCuadro
  color getColor()
  {
    return colorCuadro;
  }  

  // We return the value of valorMatar
  boolean getMuerte() 
  {
    return valorMatar;
  }    

  // We set the value of valorMatar
  void setMuerte(boolean Valor)
  {
    valorMatar = Valor;
  }

  // We set the value of colorCuadro
  void setColor(color nuevoColor)
  {
    colorCuadro = nuevoColor;
  }

  // Makes the sound play and change the color of the square randomly
  void play() 
  {

    // To avoid the replay of sounds due to doubling the speed, we check
    // if ControlReproduccion is less or equal to zero
    if (ControlReproduccion <= 0)
    {

      Nota nuevaNota;

      if (pitch > 0) 
      {        
        // Creating a new sound to play
        nuevaNota = new Nota(pitch, 0.2, out);
        // Creating a new wave
        Onda o;


        // We check if the wave has random color on its fill
        if (Opt.getColorOndasRand())
        {
          // If so, we create a new wave with randoms  color on every note played
          o = new Onda(valorX, valorY, valorAlto, valorAncho, color(random(255), random(255), random(255), 255));
        }
        else
        {
          // Else, we keep the same color with every note played
          o = new Onda(valorX, valorY, valorAlto, valorAncho, colorOnda);
        }

        // We add the wave to an array
        ondas.add(o);
      }      

      // And set the delay with the speed of the bar and 2, which is the max speed the
      // sound can get
      ControlReproduccion = (65 - velocidadBarrido);

      // Drawing the square with higher alphan than the initial color to make a kind of activate effect
      colorCuadro = color(red(colorCuadro), green(colorCuadro), blue(colorCuadro), 255);
    }
    else
    {
      // Decreasing ControlReproduccion        
      ControlReproduccion--;
    }
  }

  // Display function   
  void display() 
  {

    // We assing the speed of the bar
    velocidadBarrido = Opt.getVelocidadBarra();

    // Fill the square with the color
    fill(colorCuadro);

    // Draw the square
    rect(valorX * valorAncho, valorY * valorAlto, valorAncho, valorAlto);
  }

  void stop()
  {
    // Closing Minim audio classes when we are done with them
    out.close();
    minim.stop();
  }
}

